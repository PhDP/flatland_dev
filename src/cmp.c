#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "flatland/cmp.h"

int fl_cmp_string(const void *x, const void *y) {
  return strcmp(*(const char**)x, *(const char**)y);
}

int fl_cmp_char(const void *x, const void *y) {
  if (*(const char*)x < *(const char*)y)
    return -1;
  else if (*(const char*)x > *(const char*)y)
    return 1;
  return 0;
}

int fl_cmp_uchar(const void *x, const void *y) {
  if (*(const unsigned char*)x < *(const unsigned char*)y)
    return -1;
  else if (*(const unsigned char*)x > *(const unsigned char*)y)
    return 1;
  return 0;
}

int fl_cmp_double(const void *x, const void *y) {
  if (*(const double*)x < *(const double*)y)
    return -1;
  else if (*(const double*)x > *(const double*)y)
    return 1;
  return 0;
}

int fl_cmp_float(const void *x, const void *y) {
  if (*(const float*)x < *(const float*)y)
    return -1;
  else if (*(const float*)x > *(const float*)y)
    return 1;
  return 0;
}

int fl_cmp_int(const void *x, const void *y) {
  if (*(const int*)x < *(const int*)y)
    return -1;
  else if (*(const int*)x > *(const int*)y)
    return 1;
  return 0;
}

int fl_cmp_uint(const void *x, const void *y) {
  if (*(const unsigned int*)x < *(const unsigned int*)y)
    return -1;
  else if (*(const unsigned int*)x > *(const unsigned int*)y)
    return 1;
  return 0;
}

int fl_cmp_long(const void *x, const void *y) {
  if (*(const long*)x < *(const long*)y)
    return -1;
  else if (*(const long*)x > *(const long*)y)
    return 1;
  return 0;
}

int fl_cmp_ulong(const void *x, const void *y) {
  if (*(const unsigned long*)x < *(const unsigned long*)y)
    return -1;
  else if (*(const unsigned long*)x > *(const unsigned long*)y)
    return 1;
  return 0;
}

int fl_cmp_int8(const void* x, const void* y) {
  if (*(const int8_t*)x < *(const int8_t*)y)
    return -1;
  else if (*(const int8_t*)x > *(const int8_t*)y)
    return 1;
  return 0;
}

int fl_cmp_int16(const void* x, const void* y) {
  if (*(const int16_t*)x < *(const int16_t*)y)
    return -1;
  else if (*(const int16_t*)x > *(const int16_t*)y)
    return 1;
  return 0;
}

int fl_cmp_int32(const void* x, const void* y) {
  if (*(const int32_t*)x < *(const int32_t*)y)
    return -1;
  else if (*(const int32_t*)x > *(const int32_t*)y)
    return 1;
  return 0;
}

int fl_cmp_int64(const void* x, const void* y) {
  if (*(const int64_t*)x < *(const int64_t*)y)
    return -1;
  else if (*(const int64_t*)x > *(const int64_t*)y)
    return 1;
  return 0;
}

int fl_cmp_uint8(const void* x, const void* y) {
  if (*(const uint8_t*)x < *(const uint8_t*)y)
    return -1;
  else if (*(const uint8_t*)x > *(const uint8_t*)y)
    return 1;
  return 0;
}

int fl_cmp_uint16(const void* x, const void* y) {
  if (*(const uint16_t*)x < *(const uint16_t*)y)
    return -1;
  else if (*(const uint16_t*)x > *(const uint16_t*)y)
    return 1;
  return 0;
}

int fl_cmp_uint32(const void* x, const void* y) {
  if (*(const uint32_t*)x < *(const uint32_t*)y)
    return -1;
  else if (*(const uint32_t*)x > *(const uint32_t*)y)
    return 1;
  return 0;
}

int fl_cmp_uint64(const void* x, const void* y) {
  if (*(const uint64_t*)x < *(const uint64_t*)y)
    return -1;
  else if (*(const uint64_t*)x > *(const uint64_t*)y)
    return 1;
  return 0;
}
