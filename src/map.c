#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "flatland/common.h"
#include "flatland/map.h"
#include "flatland/set.h"
#include "flatland/fmt.h"

static void grow(fl_map *m) {
  m->capacity = m->capacity == 0? 2 : (m->capacity * 3 + 1) / 2;
  m->base = realloc(m->base, m->capacity * fl_map_pair_size(m));
}

void fl_map_init(fl_map *m, size_t key_size, size_t val_size,
                 int (*cmp)(const void*, const void*)) {
  m->base = malloc(8 * key_size + 8 * val_size);
  m->capacity = 8;
  m->nmemb = 0;
  m->key_size = key_size;
  m->val_size = val_size;
  m->cmp = cmp;
}

void fl_map_init_with_capacity(fl_map *m, size_t key_size,
                               size_t val_size, size_t capacity,
                               int (*cmp)(const void*, const void*)) {
  m->base = malloc(capacity * key_size + capacity * val_size);
  m->capacity = capacity;
  m->key_size = key_size;
  m->val_size = val_size;
  m->nmemb = 0;
  m->cmp = cmp;
}

void fl_map_copy(fl_map *dest, const fl_map *src) {
  const size_t bytes = src->capacity * fl_map_pair_size(src);
  dest->base = malloc(bytes);
  memcpy(dest->base, src->base, bytes);
  dest->capacity = src->capacity;
  dest->key_size = src->key_size;
  dest->val_size = src->val_size;
  dest->nmemb = src->nmemb;
  dest->cmp = src->cmp;
}

void fl_map_clear(fl_map *m) {
  m->nmemb = 0;
}

bool fl_map_reserve(fl_map *m, size_t new_capacity) {
  if (m->capacity < new_capacity) {
    m->base = realloc(m->base, new_capacity * fl_map_pair_size(m));
    m->capacity = new_capacity;
    return true;
  }
  return false;
}

void fl_map_free(fl_map *m) {
  if (m->base)
    free(m->base);
}

bool fl_map_get(const fl_map *m, const void *key, void *value) {
  const void *ptr = bsearch(key, m->base, m->nmemb, fl_map_pair_size(m), m->cmp);
  if (value != NULL && ptr != NULL)
    memcpy(value, (void*)((uint8_t*)ptr + m->key_size), m->val_size);
  return ptr != NULL;
}

void fl_map_insert(fl_map *m, const void *key, const void *value) {
  // Insert the first element:
  if (m->nmemb == 0) {
    if (m->capacity == 0)
      grow(m);
    memcpy(m->base, key, m->key_size);
    memcpy((void*)((uint8_t*)m->base + m->key_size), value, m->val_size);
    m->nmemb++;
    return;
  }

  const size_t pair_size = fl_map_pair_size(m);
  size_t a = 0, b = m->nmemb, idx;
  uint8_t* p;
  while (a < b) {
    idx = (a + b) / 2;
    p = m->base + idx * pair_size;
    const int comparison = m->cmp(key, (void*)p);
    if (comparison < 0)
      b = idx;
    else if (comparison > 0)
      a = idx + 1;
    else { // The key is already there!
      memcpy(p + m->key_size, value, m->val_size);
      return;
    }
  }

  if (m->capacity == m->nmemb)
    grow(m);

  void *insert_ptr = (void*)((uint8_t*)m->base + a * pair_size);

  // Push back the other values if insert_ptr is not at the end of the array:
  if (a < m->nmemb) {
    const size_t block_size = (m->nmemb - a) * pair_size;
    fl_memmove_by(insert_ptr, block_size, pair_size);
  }

  // Actually insert the key/value pair:
  memcpy(insert_ptr, key, m->key_size);
  memcpy((void*)((uint8_t*)insert_ptr + m->key_size), value, m->val_size);
  m->nmemb++;
  return;
}

bool fl_map_update(fl_map *m, const void *key, const void *value) {
  if (m->nmemb == 0)
    return false;

  const size_t pair_size = fl_map_pair_size(m);
  size_t a = 0, b = m->nmemb, idx;
  uint8_t* p;
  while (a < b) {
    idx = (a + b) / 2;
    p = m->base + idx * pair_size;
    const int comparison = m->cmp(key, (void*)p);
    if (comparison < 0)
      b = idx;
    else if (comparison > 0)
      a = idx + 1;
    else // Update value
      return memcpy(p + m->key_size, value, m->val_size) != NULL;
  }
  return false;
}

bool fl_map_remove(fl_map *m, const void* key) {
  // Empty map = nothing to remove
  if (m->nmemb == 0)
    return false;

  const size_t pair_size = fl_map_pair_size(m);
  size_t a = 0, b = m->nmemb, idx;
  uint8_t* p;
  while (a < b) {
    idx = (a + b) / 2;
    p = (uint8_t*)m->base + idx * pair_size;
    const int comparison = m->cmp(key, (void*)p);
    if (comparison < 0)
      b = idx;
    else if (comparison > 0)
      a = idx + 1;
    else { // Key found, kill it, kill it with fire!!!
      m->nmemb--;
      // Unless the key to remove is the last, move memory:
      if (idx != m->nmemb) {
        const size_t block_size = (m->nmemb - idx) * pair_size;
        memmove((void*)p, (void*)((uint8_t*)p + pair_size), block_size);
      }
      return true;
    }
  }

  // Key not found, nothing to remove:
  return false;
}

char* fl_map_to_string(const fl_map *m, char* key_to_string(const void*),
                       char* val_to_string(const void*)) {
  if (m->nmemb == 0) {
    char *str = malloc(3);
    sprintf(str, "{}");
    return str;
  }

  size_t capacity = 8, used = 1, idx = 0;
  char* str = malloc(capacity);

  const size_t pair_size = fl_map_pair_size(m);
  uint8_t *it = (uint8_t*)m->base;
  uint8_t *m_end = (uint8_t*)m->base + m->nmemb * pair_size;

  while(it != m_end) {
    char* key_str = key_to_string((const void*)it);
    char* val_str = val_to_string((const void*)(it + m->key_size));

    used += strlen(key_str) + strlen(val_str) + 7; // 7 = 3 for ", " and 4 for " -> "

    while (capacity <= used + 2) { // 2 for the extra } and end character.
      capacity <<= 1;
      str = (char*)realloc((void*)str, capacity);
    }

    if (it == (uint8_t*)m->base)
      sprintf(str, "{%s -> %s", key_str, val_str);
    else
      sprintf(str, "%s, %s -> %s", str, key_str, val_str);

    free(key_str);
    free(val_str);
    it += pair_size;
  }
  sprintf(str, "%s}", str);
  return str;
}

void fl_map_print(const fl_map *m, char* key_to_string(const void*),
                  char* val_to_string(const void*), FILE *out) {
  char *str = fl_map_to_string(m, key_to_string, val_to_string);
  fprintf(out, "%s", str);
  free(str);
}

void fl_map_print_name(const fl_map *m, const char* name,
                       char* key_to_string(const void*),
                       char* val_to_string(const void*), FILE *out) {
  char *str = fl_map_to_string(m, key_to_string, val_to_string);
  fprintf(out, "%s: %s", name, str);
  free(str);
}

