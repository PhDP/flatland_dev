#include "flatland/rng.h"

uint64_t fl_rng_next(uint64_t seed) {
  uint64_t z = (seed += UINT64_C(0x9E3779B97F4A7C15));
  z = (z ^ (z >> 30)) * UINT64_C(0xBF58476D1CE4E5B9);
  z = (z ^ (z >> 27)) * UINT64_C(0x94D049BB133111EB);
  return z ^ (z >> 31);
}

double fl_uint_to_double(uint64_t n) {
  const union { uint64_t i; double d; } u = {
    .i = UINT64_C(0x3FF) << 52 | n >> 12
  };
  return u.d - 1.0;
}
