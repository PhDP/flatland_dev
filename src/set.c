#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "flatland/common.h"
#include "flatland/set.h"
#include "flatland/fmt.h"

static void grow(fl_set *s) {
  s->capacity = s->capacity == 0? 2 : (s->capacity * 3 + 1) / 2;
  s->base = realloc(s->base, s->capacity * s->key_size);
}

void fl_set_init(fl_set *s, size_t key_size,
                 int (*cmp)(const void*, const void*)) {
  s->base = malloc(8 * key_size);
  s->capacity = 8;
  s->nmemb = 0;
  s->key_size = key_size;
  s->cmp = cmp;
}

void fl_set_init_with_capacity(fl_set *s, size_t key_size, size_t capacity,
                               int (*cmp)(const void*, const void*)) {
  s->base = malloc(capacity * key_size);
  s->capacity = capacity;
  s->key_size = key_size;
  s->nmemb = 0;
  s->cmp = cmp;
}

void fl_set_copy(fl_set* dest, const fl_set *src) {
  fl_set_init_with_capacity(dest, src->key_size, src->capacity, src->cmp);
  memcpy(dest->base, src->base, src->key_size * src->nmemb);
  dest->nmemb = src->nmemb;
}

void fl_set_clear(fl_set *s) {
  s->nmemb = 0;
}

bool fl_set_reserve(fl_set *s, size_t newcapacity) {
  if (newcapacity > s->capacity) {
    s->base = realloc(s->base, newcapacity * s->key_size);
    s->capacity = newcapacity;
    return true;
  }
  return false;
}

void fl_set_free(fl_set *s) {
  if (s->base)
    free(s->base);
}

bool fl_set_insert(fl_set *s, const void* key) {
  // Insert the first element:
  if (s->nmemb == 0) {
    if (s->capacity == 0)
      grow(s);
    memcpy(s->base, key, s->key_size);
    s->nmemb++;
    return true;
  }

  // Check where to insert the key (unless it is present):

  size_t a = 0, b = s->nmemb, idx;
  void* p;
  while (a < b) {
    idx = (a + b) / 2;
    p = (void*)((uint8_t*)s->base + idx * s->key_size);
    const int comparison = s->cmp(key, p);
    if (comparison < 0)
      b = idx;
    else if (comparison > 0)
      a = idx + 1;
    else // The key is already there!
      return false;
  }

  // Insert the key:

  if (s->capacity == s->nmemb)
    grow(s);

  void *insert_ptr = (void*)((uint8_t*)s->base + a * s->key_size);

  // Push back the other values if insert_ptr is not at the end of the array:
  if (a < s->nmemb) {
    const size_t size = (s->nmemb - a) * s->key_size;
    fl_memmove_by(insert_ptr, size, s->key_size);
  }

  memcpy(insert_ptr, key, s->key_size);
  s->nmemb++;
  return true;
}

bool fl_set_remove(fl_set *s, const void* key) {
  // Empty set = nothing to remove
  if (s->nmemb == 0)
    return false;

  size_t a = 0, b = s->nmemb, idx;
  void* p;
  while (a < b) {
    idx = (a + b) / 2;
    p = (void*)((uint8_t*)s->base + idx * s->key_size);
    const int comparison = s->cmp(key, p);
    if (comparison < 0)
      b = idx;
    else if (comparison > 0)
      a = idx + 1;
    else { // Key found, kill it, kill it with fire!!!
      s->nmemb--;
      // Unless the key to remove is the last, move memory:
      if (idx != s->nmemb) {
        const size_t size = (s->nmemb - idx) * s->key_size;
        memmove(p, (void*)((uint8_t*)p + s->key_size), size);
      }
      return true;
    }
  }

  // Key not found, nothing to remove:
  return false;
}

size_t fl_set_union(const fl_set* x, const fl_set* y, fl_set* out) {
  // Not as optimized as other set operations.
  // Optimization: if out is empty, just copy the mem directly from x to out.
  // Optimization: handle special cases where x or y are empty.
  // Optimization: only one loop?

  fl_set* c;
  if (out != NULL) {
    c = out;
  } else {
    c = (fl_set*)malloc(sizeof(fl_set));
    fl_set_init_with_capacity(c, x->key_size, x->nmemb, x->cmp);
  }

  uint8_t *it = (uint8_t*)x->base;
  uint8_t *end = (uint8_t*)x->base + x->nmemb * x->key_size;
  while (it != end) {
    fl_set_insert(c, (void*)it);
    it += x->key_size;
  }

  it = (uint8_t*)y->base;
  end = (uint8_t*)y->base + y->nmemb * y->key_size;
  while (it != end) {
    fl_set_insert(c, (void*)it);
    it += y->key_size;
  }

  const size_t count = c->nmemb;
  if (out == NULL)
    free(c);
  return count;
}

size_t fl_set_intersection(const fl_set* x, const fl_set* y, fl_set* out) {
  if (x->nmemb == 0 || y->nmemb == 0)
    return 0;

  size_t count = 0;
  const size_t key_size = x->key_size;
  uint8_t *x_p = (uint8_t*)x->base;
  uint8_t *y_p = (uint8_t*)y->base;
  uint8_t *x_end = (uint8_t*)x->base + x->nmemb * x->key_size;
  uint8_t *y_end = (uint8_t*)y->base + y->nmemb * y->key_size;
  uint8_t *out_end = out? (uint8_t*)out->base : NULL;

  while (x_p != x_end && y_p != y_end) {
    const int comparison = x->cmp((const void*)x_p, (const void*)y_p);
    if (comparison == 0) {
      if (out) {
        if (out->nmemb == out->capacity)
          grow(out);

        memcpy(fl_set_pick_nth(out, out->nmemb), (void*)x_p, key_size);
        out->nmemb++;
      }
      ++count;
    }
    if (comparison <= 0)
      x_p += key_size;
    if (comparison >= 0)
      y_p += key_size;
  }
  return count;
}

size_t fl_set_difference(const fl_set* x, const fl_set* y, fl_set* out) {
  if (out)
    fl_set_copy(out, x);

  size_t count = out->nmemb;
  const size_t key_size = x->key_size;
  uint8_t *x_p = (uint8_t*)x->base;
  uint8_t *y_p = (uint8_t*)y->base;
  uint8_t *x_end = (uint8_t*)x->base + x->nmemb * x->key_size;
  uint8_t *y_end = (uint8_t*)y->base + y->nmemb * y->key_size;
  uint8_t *out_end = out? (uint8_t*)out->base : NULL;

  while (x_p != x_end && y_p != y_end) {
    const int comparison = x->cmp((const void*)x_p, (const void*)y_p);
    if (comparison == 0) {
      if (out)
        fl_set_remove(out, (const void*)x_p); // Could be faster.
      --count;
    }
    if (comparison <= 0)
      x_p += key_size;
    if (comparison >= 0)
      y_p += key_size;
  }

  return count;
}

size_t fl_set_in_difference(fl_set* x, const fl_set* y) {
  const size_t key_size = x->key_size;
  uint8_t *x_p = (uint8_t*)x->base;
  uint8_t *y_p = (uint8_t*)y->base;
  uint8_t *x_end = (uint8_t*)x->base + x->nmemb * x->key_size;
  uint8_t *y_end = (uint8_t*)y->base + y->nmemb * y->key_size;

  while (x_p != x_end && y_p != y_end) {
    const int comparison = x->cmp((const void*)x_p, (const void*)y_p);
    if (comparison == 0) {
      memmove((void*)x_p, (void*)(x_p + key_size), (size_t)(x_end - x_p + key_size));
      x->nmemb--;
    }
    if (comparison < 0)
      x_p += key_size;
    if (comparison >= 0)
      y_p += key_size;
  }

  return x->nmemb;
}

// UNTESTED!!!!!!!!
bool fl_set_subset(const fl_set* x, const fl_set* y) {
  if (x->nmemb > y->nmemb)
    return false;

  const size_t key_size = x->key_size;
  uint8_t *x_p = (uint8_t*)x->base;
  uint8_t *y_p = (uint8_t*)y->base;
  uint8_t *x_end = (uint8_t*)x->base + x->nmemb * x->key_size;
  uint8_t *y_end = (uint8_t*)y->base + y->nmemb * y->key_size;

  while (x_p != x_end && y_p != y_end) {
    const int comparison = x->cmp((const void*)x_p, (const void*)y_p);
    if (comparison < 0)
      return false;
    if (comparison == 0)
      x_p += key_size;
    if (comparison >= 0)
      y_p += key_size;
  }
  return true;
}

// TO IMPLEMENT!
int fl_set_cmp(const void *x, const void *y) {
  if (((fl_set*)x)->nmemb != ((fl_set*)y)->nmemb)
    return ((fl_set*)x)->nmemb - ((fl_set*)y)->nmemb;
  return 0;
}

bool fl_set_sorted(const fl_set *s) {
  uint8_t *i = (uint8_t*)s->base;
  uint8_t *j = i + s->key_size;
  uint8_t *end = (uint8_t*)s->base + s->nmemb * s->key_size;
  while (j != end) {
    if (s->cmp(i, j) > 0)
      return false;
    i = j;
    j += s->key_size;
  }
  return true;
}

char* fl_set_to_string(const fl_set *s, char* to_string(const void*)) {
  if (s->nmemb == 0) {
    char *str = malloc(3);
    sprintf(str, "{}");
    return str;
  }

  char* str = fl_intersperse(s->base, ", ", s->nmemb, s->key_size, to_string);
  char* final_str = (char*)malloc(strlen(str) + 3);
  sprintf(final_str, "{%s}", str);
  free(str);
  return final_str;
}

void fl_set_print(const fl_set *s, char* to_string(const void*), FILE *out) {
  char *str = fl_set_to_string(s, to_string);
  fprintf(out, "%s", str);
  free(str);
}

void fl_set_print_name(const fl_set *s, const char* name, char* to_string(const void*), FILE *out) {
  char *str = fl_set_to_string(s, to_string);
  fprintf(out, "%s: %s", name, str);
  free(str);
}

