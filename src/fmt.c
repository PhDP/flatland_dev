#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "flatland/fmt.h"

uint32_t fl_digits(uint64_t n) {
  if (n < 10)
    return 1;
  if (n < 100)
    return 2;
  if (n < 1000)
    return 3;

  if (n < 1000000000000UL) {
    if (n < 100000000UL) {
      if (n < 1000000) {
        if (n < 10000)
          return 4;
        return 5 + (n >= 100000);
      }
      return 7 + (n >= 10000000UL);
    }
    if (n < 10000000000UL)
     return 9 + (n >= 1000000000UL);
    return 11 + (n >= 100000000000UL);
  }
  return 12 + fl_digits(n / 1000000000000UL);
}

char* fl_int_to_string(const void* i) {
  const int neg = *(int*)i < 0;
  char* str = (char*)malloc(fl_digits(*(int*)i) + 1 + neg);
  sprintf(str, "%d", *(int*)i);
  return str;
}

char* fl_uint_to_string(const void* u) {
  char* str = (char*)malloc(fl_digits(*(unsigned int*)u) + 1);
  sprintf(str, "%u", *(unsigned int*)u);
  return str;
}

char* fl_double_to_string(const void* d) {
  const int neg = *(double*)d < 0;
  char* str = (char*)malloc(fl_digits((int)(*(double*)d)) + 6 + neg);
  sprintf(str, "%.4f", *(double*)d);
  return str;
}

char* fl_string_to_string(const void* s) {
  char* str = (char*)malloc(strlen((const char*)s) + 1);
  strcpy(str, (const char*)s);
  return str;
}

char* fl_intersperse(void* data, const char* between, size_t nmemb, size_t member_size, char* to_string(const void*)) {
  if (nmemb == 0)
    return NULL;
  if (nmemb == 1)
    return to_string(data);

  const size_t between_length = strlen(between);

  size_t capacity = 8, used = 0, idx = 0;
  char* str = malloc(capacity);

  for (; idx < nmemb; ++idx) {
    char* elem_str = to_string((const void*)((uint8_t*)data + idx * member_size));
    used += strlen(elem_str) + between_length;

    while (capacity <= used) {
      capacity <<= 1;
      str = (char*)realloc((void*)str, capacity);
    }

    if (idx == 0)
      sprintf(str, "%s", elem_str);
    else
      sprintf(str, "%s%s%s", str, between, elem_str);

    free(elem_str);
  }
  return str;
}
