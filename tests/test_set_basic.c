#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "flatland/set.h"
#include "flatland/cmp.h" // For comparison functions
#include "flatland/fmt.h" // For formatting

void fl_print_int_set(const fl_set *s) {
  char* str = fl_set_to_string(s, fl_int_to_string);
  printf("%s\n", str);
  free(str);
}

int main() {
  fl_set ps; // Will store perfect numbers
  fl_set_init(&ps, sizeof(int), fl_cmp_int);

  assert(ps.nmemb == 0);

  fl_print_int_set(&ps);

  // Insert a first key:
  int n = 6;
  fl_set_insert(&ps, (void*)&n);
  assert(ps.nmemb == 1);
  assert(fl_set_has_key(&ps, (void*)&n));

  fl_print_int_set(&ps);

  // Insert a second key:
  n = 28;
  assert(!fl_set_has_key(&ps, (void*)&n));
  fl_set_insert(&ps, (void*)&n);
  assert(ps.nmemb == 2);
  assert(fl_set_has_key(&ps, (void*)&n));

  fl_print_int_set(&ps);

  // Cannot insert the same key twice:
  fl_set_insert(&ps, (void*)&n);
  fl_set_insert(&ps, (void*)&n);
  assert(ps.nmemb == 2);
  assert(fl_set_has_key(&ps, (void*)&n));

  fl_print_int_set(&ps);

  // Insert a third key:
  n = 8128;
  assert(!fl_set_has_key(&ps, (void*)&n));
  fl_set_insert(&ps, (void*)&n);
  assert(ps.nmemb == 3);
  assert(fl_set_has_key(&ps, (void*)&n));

  fl_print_int_set(&ps);

  // Insert a fourth key in the middle:
  n = 496;
  assert(!fl_set_has_key(&ps, (void*)&n));
  fl_set_insert(&ps, (void*)&n);
  assert(ps.nmemb == 4);
  assert(fl_set_has_key(&ps, (void*)&n));

  fl_print_int_set(&ps);

  // Insert a fifth key at the beginning:
  n = 1;
  assert(!fl_set_has_key(&ps, (void*)&n));
  fl_set_insert(&ps, (void*)&n);
  assert(ps.nmemb == 5);
  assert(fl_set_has_key(&ps, (void*)&n));

  fl_print_int_set(&ps);

  // The number that should be in the set:
  int numbers[5] = {1, 6, 28, 496, 8128};

  // These keys cannot be inserted:
  size_t idx;
  for (idx = 0; idx < 5; ++idx) {
    void *ptr = (void*)&numbers[idx];
    assert(fl_set_has_key(&ps, ptr));
    assert(!fl_set_insert(&ps, ptr));
    assert(ps.nmemb == 5);
  }

  // numbers is exactly the same thing as ps->base.
  assert(memcmp(ps.base, (void*)&numbers[0], 5 * sizeof(int)) == 0);

  n = 1;
  assert(fl_set_has_key(&ps, &n));
  assert(ps.nmemb == 5);
  assert(fl_set_remove(&ps, &n));
  assert(ps.nmemb == 4);

  fl_print_int_set(&ps);

  n = 1;
  assert(!fl_set_has_key(&ps, &n));
  assert(!fl_set_remove(&ps, &n));
  assert(ps.nmemb == 4);

  fl_print_int_set(&ps);

  n = 28;
  assert(fl_set_has_key(&ps, &n));
  assert(fl_set_remove(&ps, &n));
  assert(ps.nmemb == 3);

  fl_print_int_set(&ps);

  n = 8128;
  assert(fl_set_has_key(&ps, &n));
  assert(fl_set_remove(&ps, &n));
  assert(ps.nmemb == 2);

  fl_print_int_set(&ps);

  n = 496;
  assert(fl_set_has_key(&ps, &n));
  assert(fl_set_remove(&ps, &n));
  assert(ps.nmemb == 1);

  fl_print_int_set(&ps);

  n = 6;
  assert(fl_set_has_key(&ps, &n));
  assert(fl_set_remove(&ps, &n));
  assert(ps.nmemb == 0);

  fl_print_int_set(&ps);

  n = 6;
  assert(!fl_set_has_key(&ps, &n));
  assert(!fl_set_remove(&ps, &n));
  assert(ps.nmemb == 0);

  fl_print_int_set(&ps);

  return EXIT_SUCCESS;
}
