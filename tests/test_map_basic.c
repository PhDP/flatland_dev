#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include "flatland/map.h"
#include "flatland/cmp.h" // For comparison functions
#include "flatland/fmt.h" // For formatting

int main() {
  fl_map xs; // Will map ints to double
  fl_map_init(&xs, sizeof(uint32_t), sizeof(double), fl_cmp_uint32);

  assert(xs.nmemb == 0);

  fl_map_print(&xs, fl_int_to_string, fl_double_to_string, stdout);
  printf("\n");

  // Insert first element:
  uint32_t key = 42;
  double value = 6.48074069840786;
  fl_map_insert(&xs, &key, &value);
  assert(xs.nmemb == 1);
  fl_map_print(&xs, fl_int_to_string, fl_double_to_string, stdout);
  printf("\n");

  // Insert in front:
  key = 2;
  value = 1.4142135623730951;
  fl_map_insert(&xs, &key, &value);
  assert(xs.nmemb == 2);
  fl_map_print(&xs, fl_int_to_string, fl_double_to_string, stdout);
  printf("\n");

  // Insert at the end:
  key = 496;
  value = 22.271057451320086;
  fl_map_insert(&xs, &key, &value);
  assert(xs.nmemb == 3);
  fl_map_print(&xs, fl_int_to_string, fl_double_to_string, stdout);
  printf("\n");

  // Insert in the middle:
  key = 9;
  value = 3.0;
  fl_map_insert(&xs, &key, &value);
  assert(xs.nmemb == 4);
  fl_map_print(&xs, fl_int_to_string, fl_double_to_string, stdout);
  printf("\n");

  // Update key:
  key = 9;
  value = 0.0;
  fl_map_insert(&xs, &key, &value);
  assert(xs.nmemb == 4);
  fl_map_print(&xs, fl_int_to_string, fl_double_to_string, stdout);
  printf("\n");

  // Update key with 'update' function:
  key = 9;
  value = 3.0;
  assert(fl_map_update(&xs, &key, &value));
  assert(xs.nmemb == 4);
  fl_map_print(&xs, fl_int_to_string, fl_double_to_string, stdout);
  printf("\n");

  // Try to update a key that doesn't exist:
  key = 8;
  assert(!fl_map_update(&xs, &key, &value));
  assert(xs.nmemb == 4);
  fl_map_print(&xs, fl_int_to_string, fl_double_to_string, stdout);
  printf("\n");

  // Get
  key = 2;
  assert(fl_map_get(&xs, &key, &value));
  assert(value == 1.4142135623730951);
  printf("Key %d has value %.4f in ", key, value);
  fl_map_print(&xs, fl_int_to_string, fl_double_to_string, stdout);
  printf("\n");

  key = 9;
  assert(fl_map_get(&xs, &key, &value));
  assert(value == 3.0);
  printf("Key %d has value %.4f in ", key, value);
  fl_map_print(&xs, fl_int_to_string, fl_double_to_string, stdout);
  printf("\n");

  key = 42;
  assert(fl_map_get(&xs, &key, &value));
  assert(value == 6.48074069840786);
  printf("Key %d has value %.4f in ", key, value);
  fl_map_print(&xs, fl_int_to_string, fl_double_to_string, stdout);
  printf("\n");

  key = 496;
  assert(fl_map_get(&xs, &key, &value));
  assert(value == 22.271057451320086);
  printf("Key %d has value %.4f in ", key, value);
  fl_map_print(&xs, fl_int_to_string, fl_double_to_string, stdout);
  printf("\n");

  uint32_t in[4] = {2, 9, 42, 496};
  uint32_t not_in[3] = {1, 10, 500};

  size_t idx = 0;
  for (; idx < 3; ++idx) {
    key = not_in[idx];
    assert(!fl_map_get(&xs, &key, &value));
    printf("Key %d is not in the map ", key);
    fl_map_print(&xs, fl_int_to_string, fl_double_to_string, stdout);
    printf("\n");
  }

  // Remove

  for (idx = 0; idx < 3; ++idx) {
    key = not_in[idx];
    assert(!fl_map_remove(&xs, &key));
  }

  for (idx = 0; idx < 4; ++idx) {
    key = in[idx];
    assert(fl_map_remove(&xs, &key));
    assert(xs.nmemb == (4 - 1 - idx));
    printf("Removing key %d: ", key);
    fl_map_print(&xs, fl_int_to_string, fl_double_to_string, stdout);
    printf("\n");
  }

  for (idx = 0; idx < 4; ++idx) {
    key = in[idx];
    assert(!fl_map_remove(&xs, &key));
    assert(xs.nmemb == 0);
  }

  return EXIT_SUCCESS;
}
