#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <math.h>
#include "flatland/set.h"
#include "flatland/cmp.h" // For comparison functions
#include "flatland/fmt.h" // For formatting

void fl_print_double_set(const fl_set *s) {
  char* str = fl_set_to_string(s, fl_double_to_string);
  printf("%s\n", str);
  free(str);
}

int main() {
  fl_set roots;
  fl_set even_roots;
  fl_set odd_roots;

  fl_set_init(&roots, sizeof(double), fl_cmp_double);
  fl_set_init(&even_roots, sizeof(double), fl_cmp_double);
  fl_set_init(&odd_roots, sizeof(double), fl_cmp_double);

  assert(roots.nmemb == 0);
  assert(even_roots.nmemb == 0);
  assert(odd_roots.nmemb == 0);

  fl_print_double_set(&even_roots);

  size_t i = 0;
  for (; i < 20; ++i) {
    const double root = sqrt((double)i);
    assert(fl_set_insert(&even_roots, (void*)&root));
    assert(even_roots.nmemb == i + 1);
    fl_print_double_set(&even_roots);
  }

  // Remove roots of odd numbers and put them in the odd set:
  for (i = 1; i < 20; i += 2) {
    const double root = sqrt((double)i);
    assert(fl_set_remove(&even_roots, (void*)&root));
    fl_print_double_set(&even_roots);
    assert(fl_set_insert(&odd_roots, (void*)&root));
    fl_print_double_set(&odd_roots);
  }

  assert(even_roots.nmemb == 10);
  assert(odd_roots.nmemb == 10);

  // Union of two sets:
  assert(roots.nmemb == 0);
  assert(fl_set_union(&even_roots, &odd_roots, NULL) == 20);
  assert(fl_set_union(&even_roots, &odd_roots, &roots) == 20);
  assert(roots.nmemb == 20);
  fl_print_double_set(&roots);
  assert(fl_set_intersection(&even_roots, &odd_roots, NULL) == 0);
  assert(fl_set_intersection(&roots, &odd_roots, NULL) == 10);
  assert(fl_set_intersection(&roots, &even_roots, NULL) == 10);
  assert(fl_set_intersection(&even_roots, &even_roots, NULL) == 10);
  assert(fl_set_intersection(&odd_roots, &odd_roots, NULL) == 10);
  assert(fl_set_intersection(&roots, &roots, NULL) == 20);

  return EXIT_SUCCESS;
}
