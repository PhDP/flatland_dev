#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h> // To seed the random number generator
#include <flatland/set.h> // The flat set.
#include <flatland/cmp.h> // Comparison functions.
#include <flatland/fmt.h> // Functions to format and print sets.
#include <flatland/rng.h> // Elementary random number generator.

// Fill the set with numbers in the [0, max) range until it reaches
// size 'nmemb'. Returns the new state of the random number generator.
uint64_t random_set(fl_set *s, size_t nmemb, size_t max, size_t seed) {
  while (s->nmemb < nmemb) {
    seed = fl_rng_next(seed);
    const uint64_t number = seed % max;
    fl_set_insert(s, (void*)&number);
  }
  return seed;
}

void show_set(fl_set *s, const char* name) {
  fl_set_print_name(s, name, fl_uint_to_string, stdout);
  printf("\n");
}

int main(int argc, char **argv) {
  // The size of the sets.
  const size_t set_size = argc > 1? atoi(argv[1]) : 32;

  // The max value (must be higher than set_size).
  const size_t set_max = argc > 2? atoi(argv[2]) : 64;

  if (set_max < set_size) {
    printf("Trying to generate sets of size %lu in the [0, %lu) range.\n", set_size, set_max);
    printf("The range must be greater than the size of the sets.\n");
    return 0;
  }

  fl_set a, b;
  fl_set_init(&a, sizeof(uint64_t), fl_cmp_uint64);
  fl_set_init(&b, sizeof(uint64_t), fl_cmp_uint64);

  show_set(&a, "a");
  show_set(&b, "b");

  printf("\nFill sets a and b with %lu numbers in the [0, %lu) range...\n\n", set_size, set_max);

  uint64_t rng = time(NULL);

  rng = random_set(&a, set_size, set_max, rng);
  rng = random_set(&b, set_size, set_max, rng);

  show_set(&a, "a");
  show_set(&b, "b");

  printf("\nCheck for the presence of numbers in the [0, 10] range in set a.\n\n");

  for (uint64_t i = 0; i < 10; ++i) {
    if (fl_set_has_key(&a, (void*)&i))
      printf("%lu is in the set.\n", i);
    else
      printf("%lu is not in the set.\n", i);
  }

  printf("\nGet the union, intersection, and difference of a and b...\n\n");

  fl_set ab_union, ab_inter, ab_diff;
  fl_set_init(&ab_union, sizeof(uint64_t), fl_cmp_uint64);
  fl_set_init(&ab_inter, sizeof(uint64_t), fl_cmp_uint64);
  fl_set_init(&ab_diff, sizeof(uint64_t), fl_cmp_uint64);

  fl_set_union(&a, &b, &ab_union);
  fl_set_intersection(&a, &b, &ab_inter);
  fl_set_difference(&a, &b, &ab_diff);
  fl_set_in_difference(&a, &b);

  show_set(&ab_union, "The union of 'a' and 'b': ");
  show_set(&ab_inter, "The intersection of 'a' and 'b': ");
  show_set(&ab_diff, "The difference of 'a' and 'b': ");
  show_set(&a, "In-place difference of 'a' and 'b': ");

  // Free memory:
  fl_set_free(&a);
  fl_set_free(&b);
  fl_set_free(&ab_union);
  fl_set_free(&ab_inter);
  fl_set_free(&ab_diff);

  return 0;
}
