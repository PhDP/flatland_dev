#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h> // To seed the random number generator
#include <flatland/map.h> // The flat set.
#include <flatland/cmp.h> // Comparison functions.
#include <flatland/fmt.h> // Functions to format and print sets.
#include <flatland/rng.h> // Elementary random number generator.

// Fill the map with numbers in the [0, max) range, mapped to a float in the
// [0, 1) range, until it reaches size 'nmemb'. Returns the new state of the
// random number generator.
uint64_t random_map(fl_map *m, size_t nmemb, size_t max, size_t seed) {
  while (m->nmemb < nmemb) {
    seed = fl_rng_next(seed);
    const uint64_t key = seed % max;

    seed = fl_rng_next(seed);
    const double value = fl_uint_to_double(seed);

    fl_map_insert(m, (void*)&key, (void*)&value);
  }
  return seed;
}

void show_map(fl_map *s, const char* name) {
  fl_map_print_name(s, name, fl_uint_to_string, fl_double_to_string, stdout);
  printf("\n");
}

int main(int argc, char **argv) {
  // The size of the maps.
  const size_t map_size = argc > 1? atoi(argv[1]) : 32;

  // The max value (must be higher than map_size).
  const size_t map_max = argc > 2? atoi(argv[2]) : 64;

  if (map_max < map_size) {
    printf("Trying to generate maps of size %lu in the [0, %lu) range.\n", map_size, map_max);
    printf("The range must be greater than the size of the sets.\n");
    return 0;
  }

  fl_map x, y;
  fl_map_init(&x, sizeof(uint64_t), sizeof(double), fl_cmp_uint64);
  fl_map_init(&y, sizeof(uint64_t), sizeof(double), fl_cmp_uint64);

  show_map(&x, "x");
  show_map(&y, "y");

  printf("\nFill maps a and b with %lu numbers in the [0, %lu) range...\n\n", map_size, map_max);

  uint64_t rng = time(NULL);

  rng = random_map(&x, map_size, map_max, rng);
  rng = random_map(&y, map_size, map_max, rng);

  show_map(&x, "x");
  show_map(&y, "y");

  printf("\nCheck for the presence of numbers in the [0, 10] range in map a.\n\n");

  for (uint64_t i = 0; i < 10; ++i) {
    double v;
    if (fl_map_get(&x, (void*)&i, (void*)&v))
      printf("%lu is in the map x with value %f.\n", i, v);
    else
      printf("%lu is not in the map x.\n", i);
  }

  // Free memory:
  fl_map_free(&x);
  fl_map_free(&y);

  return 0;
}
