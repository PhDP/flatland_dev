/*!
  \file A map (an associated container linking keys to values).
 */
#ifndef FLATLAND_MAP_H__
#define FLATLAND_MAP_H__

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
  \brief A map as a sorted array.
 */
typedef struct {
  /** \brief The continuous array of data. */
  void* base;

  /** \brief Number of keys in the set. */
  size_t nmemb;

  /** \brief Size (in bytes) of a single key. */
  size_t key_size;

  /** \brief Size (in bytes) of a single value. */
  size_t val_size;

  /** \brief Capacity (not in bytes, but in number of key/value pairs). */
  size_t capacity;

  /** \brief Comparison function for the keys to keep the array sorted. */
  int (*cmp)(const void*, const void*);
} fl_map;

/**
  \brief Initializes a fl_map struct.

  \param m          The map to initialize.
  \param key_size   Size (in bytes) of each keys.
  \param val_size   Size (in bytes) of each values.
  \param cmp        Comparison function used to keep the keys sorted.
 */
void fl_map_init(fl_map *m, size_t key_size, size_t val_size,
                 int (*cmp)(const void*, const void*));

/**
  \brief Initializes a fl_map struct with a given starting capacity.

  \param m          The map to initialize.
  \param key_size   Size (in bytes) of each keys.
  \param val_size   Size (in bytes) of each values.
  \param capacity   Initial capacity (in number of key/value pairs).
  \param cmp        Comparison function used to keep the keys sorted.
 */
void fl_map_init_with_capacity(fl_map *m, size_t key_size,
                               size_t val_size, size_t capacity,
                               int (*cmp)(const void*, const void*));

/**
  \brief Creates a copy of the fl_map with an indepenedant array of keys/values.

  \param dest   Destination: an uninitialized fl_map.
  \param src    The map to copy.
 */
void fl_map_copy(fl_map* dest, const fl_map *mrc);

/**
  \brief Clear the map. In practice, this function simply sets the nmemb to 0,
         it does not free memory.
 */
void fl_map_clear(fl_map *m);

/**
  \brief Checks if the set has a certain key.
 */
#define fl_map_has_key(m, key)    (bsearch((key), (m)->base, (m)->nmemb, (m)->key_size + (m)->val_size, (m)->cmp) != NULL)

#define fl_map_pair_size(m)       ((m)->key_size + (m)->val_size)

/**
  \brief Get the value associated with a key, return whether the key
         was found. If value is NULL, this function will essentially
         behave like fl_map_has_key.

  \param m        The map structure.
  \param key      The key to find.
  \param value    Whether to store the value.
  \return         Whether the key was found.
 */
bool fl_map_get(const fl_map *m, const void *key, void *value);

/**
  \brief Reserves memory inside the map. Does nothing if the capacity is below
         equal the current capacity.

  \return Whether the capacity has changed.
 */
bool fl_map_reserve(fl_map *m, size_t new_capacity);

/**
  \brief Free memory inside the fl_map.
 */
void fl_map_free(fl_map *m);

/**
  \brief Inserts a new key/value pair. If the key is present, this
         function will replace the value associated with it. If
         you want to update an existing key and make sure the key
         is already there, use the fl_map_update function.
 */
void fl_map_insert(fl_map *m, const void *key, const void *value);

/**
  \brief Update the value for a given key, return whether the key was found.
 */
bool fl_map_update(fl_map *m, const void *key, const void *value);

/**
  \brief Remove a key/value pair, returns whether something was removed.
 */
bool fl_map_remove(fl_map *m, const void* key);

char* fl_map_to_string(const fl_map *m, char* key_to_string(const void*),
                       char* val_to_string(const void*));

void fl_map_print(const fl_map *m, char* key_to_string(const void*),
                  char* val_to_string(const void*), FILE *out);

void fl_map_print_name(const fl_map *m, const char* name,
                       char* key_to_string(const void*),
                       char* val_to_string(const void*), FILE *out);


#ifdef __cplusplus
}
#endif

#endif
