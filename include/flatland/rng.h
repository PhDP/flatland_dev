/*!
  \file Very elementary functions to generate random numbers. See
        https://github.com/PhDP/Randamu/ for better generators.
 */
#ifndef FLATLAND_RNG_H__
#define FLATLAND_RNG_H__

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/** \brief Generates a random number given a seed with the simple splitmix64 method. */
uint64_t fl_rng_next(uint64_t seed);

/** \brief Converts a uint64_t into a double in the [0, 1) semi-closed range. */
double fl_uint_to_double(uint64_t n);

#ifdef __cplusplus
}
#endif

#endif
