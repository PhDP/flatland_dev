#ifndef FLATLAND_COMMON_H_
#define FLATLAND_COMMON_H_

#ifdef __cplusplus
extern "C" {
#endif

#define FLATLAND         "0.1.0"
#define FLATLAND_MAJOR   0
#define FLATLAND_MINOR   1
#define FLATLAND_PATCH   0

#include <stdint.h>
#include <string.h>

/** \brief Returns a void pointer n bytes further a void pointer. */
#define fl_forward(p, n)     (void*)((uint8_t*)(p) + (n))

/**
  \brief Move a block of memory by some bytes.

  \param src      Pointer to the source.
  \param size     Size of the block of memory to move.
  \param by       Move the block by how many bytes.
 */
#define fl_memmove_by(src, size, by)     memmove((void*)((uint8_t*)(src) + by), (src), (size))

#ifdef __cplusplus
}
#endif

#endif
