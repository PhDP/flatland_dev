/*!
  \file
  \brief The functions compare int, double, int, long, etc. They take two
         variables and return a number below 0 if the first should be placed
         before the second, 0 if they are equal, and a number greater than 0 if
         the first should be placed after the second.
 */

#ifndef FLATLAND_CMP_H_
#define FLATLAND_CMP_H_

#ifdef __cplusplus
extern "C" {
#endif

int fl_cmp_string(const void*, const void*);
int fl_cmp_char(const void*, const void*);
int fl_cmp_float(const void*, const void*);
int fl_cmp_double(const void*, const void*);
int fl_cmp_int(const void*, const void*);
int fl_cmp_uint(const void*, const void*);
int fl_cmp_long(const void*, const void*);
int fl_cmp_ulong(const void*, const void*);
int fl_cmp_int8(const void*, const void*);
int fl_cmp_int16(const void*, const void*);
int fl_cmp_int32(const void*, const void*);
int fl_cmp_int64(const void*, const void*);
int fl_cmp_uint8(const void*, const void*);
int fl_cmp_uint16(const void*, const void*);
int fl_cmp_uint32(const void*, const void*);
int fl_cmp_uint64(const void*, const void*);

#ifdef __cplusplus
}
#endif

#endif
