/*!
  \file
  \brief Functions to format int, double, strings et al to get a nice
         format for sets and maps.
 */

#ifndef FLATLAND_FMT_H_
#define FLATLAND_FMT_H_

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
  \brief Count the base-10 length of a number.
 */
uint32_t fl_digits(uint64_t n);

/**
  \brief Converts an int to a string.
 */
char* fl_int_to_string(const void* i);

/**
  \brief Converts an unsigned int to a string.
 */
char* fl_uint_to_string(const void* u);

/**
  \brief Converts a double to a string.
 */
char* fl_double_to_string(const void* d);

/**
  \brief Copies a string...
 */
char* fl_string_to_string(const void* s);

/**
  \brief Intersperse the elements of a set with some string.

  \param data           Vector of data.
  \param between        The string to put between elements, e.g. ", ".
  \param nmemb          Number of members in 'data'.
  \param member_size    Size (in bytes) of an element in 'data'.
  \param to_string      Takes an element from data, format it, and return the string.
  \return               The elements in data formatted and interspersed with 'between'.
 */
char* fl_intersperse(void* data, const char* between, size_t nmemb, size_t member_size, char* to_string(const void*));

#ifdef __cplusplus
}
#endif

#endif
