/*!
  \file A set represented by a sorted array.
 */
#ifndef FLATLAND_SET_H__
#define FLATLAND_SET_H__

#include <stdlib.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
  \brief A set as a sorted array.
 */
typedef struct {
  /** \brief The continuous array of data. */
  void* base;

  /** \brief Number of keys in the set. */
  size_t nmemb;

  /** \brief Size (in bytes) of a single key. */
  size_t key_size;

  /** \brief Capacity (not in bytes, but in number of keys). */
  size_t capacity;

  /** \brief Comparison function to keep the array sorted. */
  int (*cmp)(const void*, const void*);
} fl_set;

/**
  \brief Initializes a fl_set struct.

  \param s          The set to initialize.
  \param key_size   Size (in bytes) of each keys.
  \param cmp        Comparison function used to keep the keys sorted.
 */
void fl_set_init(fl_set *s, size_t key_size,
                 int (*cmp)(const void*, const void*));

/**
  \brief Initializes a fl_set struct.

  \param s          The set to initialize.
  \param key_size   Size (in bytes) of each keys.
  \param capacity   Initial capacity (in number of keys).
  \param cmp        Comparison function used to keep the keys sorted.
 */
void fl_set_init_with_capacity(fl_set *s, size_t key_size, size_t capacity,
                               int (*cmp)(const void*, const void*));

/**
  \brief Creates a copy of the fl_set with an indepenedant array of keys.

  \param dest   Destination: an uninitialized fl_set.
  \param src    The set to copy.
 */
void fl_set_copy(fl_set* dest, const fl_set *src);

/**
  \brief Clear the set. In practice, this function simply sets the nmemb to 0,
         it does not free memory.
 */
void fl_set_clear(fl_set *s);

/**
  \brief Reserves memory inside the set. Does nothing if the newcapacity is
         below or equal the current capacity.

  \return       Whether the capacity has changed.
 */
bool fl_set_reserve(fl_set *s, size_t newcapacity);

/**
  \brief Free memory inside the fl_set
 */
void fl_set_free(fl_set *s);

/**
  \brief Call 'free' on all the keys.
 */
void fl_set_free_keys(fl_set *s);

/**
  \brief Inserts a new key (unless it is already present).

  \return Whether the key was added.
 */
bool fl_set_insert(fl_set *s, const void* key);

/**
  \brief Removes an key (unless it is not found).

  \return Whether the key was removed.
 */
bool fl_set_remove(fl_set *s, const void* key);

/**
  \brief Checks if the set has a certain key.
 */
#define fl_set_has_key(s, key)    (bsearch((key), (s)->base, (s)->nmemb, (s)->key_size, (s)->cmp) != NULL)

/**
  \brief Returns a pointer to the nth keys in the flat set.
 */
#define fl_set_pick_nth(s, n)     ((void*)((uint8_t*)(s)->base + (n) * (s)->key_size))

/**
  \brief The union of two sets. If 'out' is NULL, this function will only return
         the number of keys in the union.

  \param x      A set.
  \param y      Another set.
  \param out    An initialized set.
  \return       How many keys are in the union of x and y.
 */
size_t fl_set_union(const fl_set* x, const fl_set* y, fl_set* out);

/**
  \brief The intersection of two sets. If 'out' is NULL, this function will only
         return the number of keys in the intersection.

  \param x      A set.
  \param y      Another set.
  \param out    An uninitialized set. The intersection of x and y.
  \return       How many keys are in the intersection of x and y.
 */
size_t fl_set_intersection(const fl_set* x, const fl_set* y, fl_set* out);

/**
  \brief The difference of two sets. If 'out' is NULL, this function will only
         return the number of keys in the difference.

  \param x      A set.
  \param y      Another set.
  \param out    An uninitialized set. The difference of x and y.
  \return       How many keys are in the difference of x and y.
 */
size_t fl_set_difference(const fl_set* x, const fl_set* y, fl_set* out);

/**
  \brief An in-place version of set_difference. Removes the keys from set y in
         set x.

  \param x      A set to remove the keys from.
  \param y      The set of keys to remove.
  \return       How many keys are in the difference of x and y.
 */
size_t fl_set_in_difference(fl_set* x, const fl_set* y);

/**
  \brief        Returns true if x is a subset of y (or if they are equal).

  \param x      The subset.
  \param y      The superset.
  \return       How many keys are in the difference of x and y.
 */
bool fl_set_subset(const fl_set* x, const fl_set* y);

/**
  \brief Standard comparison function. Will return a negative number if x has
         fewer keys and a positive number if if has more keys. If the sets have
         the same number of keys, the keys will be compared one by one and the
         result of the first non-equal comparison will be returned. If all the
         keys are the same, return 0.
 */
int fl_set_cmp(const void *x, const void *y);

/**
  \brief Checks if the set is correctly sorted.
 */
bool fl_set_sorted(const fl_set *s);

/**
  \brief Returns a string representation of the set.

  \param s            To set.
  \param to_string    A function that takes an key of the set and format it.
                      <flatland/fmt.h> provides a few to_string functions for
                      basic types.
  \return A string with the formatted set.
 */
char* fl_set_to_string(const fl_set *s, char* to_string(const void*));

/**
  \brief Print the set to file (or the console with out = stdout).

  \param s            To set.
  \param to_string    A function that takes an key of the set and format it.
                      <flatland/fmt.h> provides a few to_string functions for
                      basic types.
 */
void fl_set_print(const fl_set *s, char* to_string(const void*), FILE *out);

/**
  \brief Print the set to file (or the console with out = stdout) with a name.

  \param s            To set.
  \param to_string    A function that takes an key of the set and format it.
                      <flatland/fmt.h> provides a few to_string functions for
                      basic types.
 */
void fl_set_print_name(const fl_set *s, const char* name, char* to_string(const void*), FILE *out);

#ifdef __cplusplus
}
#endif

#endif
