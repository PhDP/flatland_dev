# Flatland

[![Build status](https://travis-ci.org/PhDP/Flatland.svg?branch=master)](https://travis-ci.org/PhDP/Flatland)

Flatland is a small C library that implements generic flat containers for sets
and maps (like boost::container::flat_set and boost::container::flat_map, but
in C). The idea is to represent sets/maps as sorted vectors, yielding much
better data locality at the cost of slower in insertions / deletions.

It's not as pretty as the C++ flat containers... but it's fast and can be used
from pretty much everywhere.



``` C
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h> // To seed the random number generator
#include <flatland/set.h> // The flat set.
#include <flatland/cmp.h> // Comparison functions.
#include <flatland/fmt.h> // Functions to format and print sets.

// A simple random number generator we'll use to generate random sets.
uint64_t splitmix64(uint64_t seed) {
  uint64_t z = (seed += UINT64_C(0x9E3779B97F4A7C15));
  z = (z ^ (z >> 30)) * UINT64_C(0xBF58476D1CE4E5B9);
  z = (z ^ (z >> 27)) * UINT64_C(0x94D049BB133111EB);
  return z ^ (z >> 31);
}

// Fill the set with numbers in the [0, max) range until it reaches
// size 'nmemb'. Returns the new state of the random number generator.
uint64_t fillset(fl_set &s, size_t nmemb, size_t max, size_t seed) {
  while (s->nmemb < nmemb) {
    seed = splitmix(seed);
    const uint64_t number = seed % max;
    fl_set_insert(s, (void*)&number)
  }
  return seed;
}

int main(int argc, char **argv) {
  // The
  const size_t set_size = argc > 1? atoi(argv[1]) : 32;
  const size_t set_max = argc > 2? atoi(argv[2]) : 16;

}

```

# Usage

## Inserting and deleting keys in a set

``` C
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <flatland/set.h>
#include <flatland/cmp.h> // For comparison functions
#include <flatland/fmt.h> // For formatting

int main() {
  int n = 6;

  fl_set ps; // Will store perfect numbers
  fl_set_init(&ps, sizeof(int), fl_cmp_int);

  fl_set_insert(&ps, (void*)&n);

  n = 28;
  fl_set_insert(&ps, (void*)&n);

  n = 496;
  fl_set_insert(&ps, (void*)&n);

  n = 8128;
  fl_set_insert(&ps, (void*)&n);

  // Will not insert:
  n = 8128;
  fl_set_insert(&ps, (void*)&n);


  return EXIT_SUCCESS;
}

```

...will compile with:

    $ clang -Wall -std=c99 -O3 perfects.c -o perfects -lflatland

## Set union, intersection, and difference

...

# Installing

On Linux or UNIX-like systems:

    $ git clone https://github.com/PhDP/Flatland.git
    $ cd Flatland
    $ mkdir build && cd $_
    $ cmake ..
    $ make
    $ sudo make install

Flatland will, by default,

    $ make test

after 'make'.

You can generate Visual Studio project files with cmake, which can
be installed with [chocolatey](https://chocolatey.org/), and using the
command

    $ cmake .

at the root of the randamu directory. To run the tests, simply build
the RUN_TESTS project in Visual Studio.



# License

[MIT](http://opensource.org/licenses/MIT)

